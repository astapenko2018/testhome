import requests
import pytest

"""
Написать тест, который проверяет что цвет из https://reqres.in/api/unknow/2, 
присутсвует в общем списке https://reqres.in/api/unknow/
{"
data":{
"id":2,
"name":"fuchsia rose",
"year":2001,
"color":"#C74375",
"pantone_value":"17-2031"},
"support":{"url":"https://reqres.in/#support-heading",
"text":"To keep ReqRes free, contributions towards server costs are appreciated!"}}
"""

BASE_URL = "https://reqres.in"


def test_specific_colour_exists_in_full_list():
    single_color_response = requests.get(f"{BASE_URL}/api/unknown/2")
    assert single_color_response.status_code == 200, "Ошибка при получении данных о цвете"
    single_color_data = single_color_response.json()['data']

    all_colors_response = requests.get(f"{BASE_URL}/api/unknown/")
    assert all_colors_response.status_code == 200, "Ошибка при получении данных о всех цветах"
    all_color_list = all_colors_response.json()['data']

    assert single_color_data in all_color_list, f"Цвет {single_color_data['name']} не найден в полном списке"


# Вариант если id разные с помощью pop

def test_specific_colour_exists_in_full_list_2():
    single_color_response = requests.get(f"{BASE_URL}/api/unknown/2")
    assert single_color_response.status_code == 200, "Ошибка при получении данных о цвете"
    single_color_data = single_color_response.json()['data']
    single_color_data.pop('id', None)  # Метод POP

    all_colors_response = requests.get(f"{BASE_URL}/api/unknown/")
    assert all_colors_response.status_code == 200, "Ошибка при получении данных о всех цветах"
    all_color_list = all_colors_response.json()['data']
    for color in all_color_list:
        color.pop('id', None)
    assert single_color_data in all_color_list, f"Цвет {single_color_data['name']} не найден в полном списке"


# Вариант 3 если id разные самый оптимальный

def test_specific_colour_exists_in_full_list_3():
    single_color_response = requests.get(f"{BASE_URL}/api/unknown/2")
    assert single_color_response.status_code == 200, "Ошибка при получении данных о цвете"
    single_color_data = single_color_response.json()['data']
    print(single_color_data)

    all_colors_response = requests.get(f"{BASE_URL}/api/unknown/")
    assert all_colors_response.status_code == 200, "Ошибка при получении данных о всех цветах"
    all_color_list = all_colors_response.json()['data']

    found = False
    # Создаем цикл который перебирает цвета в списке
    for color in all_color_list:
        # используем функцию all Функция All возвращает True, если все эл-ты в итерируемом типе являются истинным
        if all(single_color_data[k] == color[k] for k in single_color_data if k != 'id' and k in color):
            found = True
            break
    assert single_color_data in all_color_list, f"Цвет {single_color_data['name']} не найден в полном списке"
